// Includes
#include <cstdlib>
#include <random>
#include <vector>
#include <ctime>
#include <chrono>
#include <iostream>

#include <Windows.h>

// Don't care about these, will be moved out of here later
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "dxguid.lib")

// Windows's callback
LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_DESTROY)
		std::exit(0);

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

// Creates a window on Windows ( ahaha that joke )
HWND create_window()
{
	static TCHAR szWindowClass[] = TEXT("win32app");
	static TCHAR szTitle[] = TEXT("SSA");

	WNDCLASS wndClass;
	wndClass.style = CS_DBLCLKS;
	wndClass.lpfnWndProc = StaticWndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = GetModuleHandle(NULL);
	wndClass.hIcon = LoadIcon(nullptr, MAKEINTRESOURCE(IDI_APPLICATION));
	wndClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = nullptr;
	wndClass.lpszClassName = szWindowClass;

	if (!RegisterClass(&wndClass))
	{
		DWORD dwError = GetLastError();
		return nullptr;
	}

	HWND hWnd = CreateWindow(
		szWindowClass,
		szTitle,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		1280, 720,
		NULL,
		NULL,
		GetModuleHandle(NULL),
		NULL
		);

	ShowWindow(hWnd, SW_SHOW);

	if (!hWnd)
		return nullptr;
	return hWnd;
}

// Includes will be reduced to a single header per-module
#include "graphics/ssa_render_device.hpp"
#include "utility/ssa_loaders.hpp"
#include "graphics/ssa_blender.hpp"
#include "graphics/ssa_texture.hpp"
#include "core/ssa_entry_point.hpp"

#include "graphics/2d/ssa_renderer2d.hpp"
#include "graphics/2d/ssa_sprite.hpp"

#include "graphics/ssa_post_process_effect.hpp"
#include "graphics/effects/ssa_pp_horizontal_blur_pass.hpp"
#include "graphics/effects/ssa_pp_vertical_blur_pass.hpp"

// Application entry point
int entry_point(cmd_args_t& p_args, SSA::Platform p_platform)
{
	// Just for ease of use
	using namespace SSA;

	// We create a window
	HWND window_handle = create_window();

	// The RenderDevice is the main interface for all graphic calls
	RenderDevice render_device;
	if (!render_device.init(0)) // 0 is the index of the graphics card ( usually called adapter ) [ other adapters are not supported yet, parameter is just ignored for now ]
		return false;

	// Let's create a texture
	// We are using a module ( contained in ssa_loaders.hpp ) that supports only png for now
	// We first load it
	loaded_data texture_data;
	unsigned long width, height;
	if (!load_png("sample_texture.png", texture_data, width, height))
		return false;

	loaded_data texture_data2;
	unsigned long width2, height2;
	if (!load_png("to_bloom.png", texture_data2, width2, height2))
		return false;

	// Then use the data obtained ( raw pixels ) to create it
	Texture texture(render_device); // This is just the handle you will use ( also contains informations about the texture ) 
	// If you wanna know more about this function, well just don't care for now, there is no CrossPlatform Format system yet ( sorry )
	if (!render_device.create_texture(width, height, Format::RGBA8Unorm, false, &texture_data[0], "texture_data", texture))
		return false;

	Texture texture2(render_device);
	if (!render_device.create_texture(width2, height2, Format::RGBA8Unorm, false, &texture_data2[0], "texture_data2", texture2))
		return false;

	// We have created a window, but as with the texture, the window itself means nothing to the might Stegosaurus
	// That's why you have to convert it to a Stegosaurus object
	Texture render_window(render_device); // A RenderWindow is a window where you render onto ( uahuuu ) 
	if (!render_device.create_render_window(window_handle, "my_super_special_window", render_window))
		return false;

	// We now create a Renderer2D that has the duty to render things on screen
	// We will later have multiple renderers for different purposes ( deferred, prepass, 2d, 2.5d & more ) 
	Renderer2D renderer2d(render_device);

	Blender alpha_blend(render_device);
	if (!render_device.create_blender("blender", alpha_blend))
		return false;

	Sprite sprite;
	sprite.position = float2(500, 300);
	sprite.set_texture(texture2);
	sprite.set_color(float4(1.f, 1.f, 1.f, 1.f));

	auto now = std::chrono::high_resolution_clock::now();
	unsigned int ticks{ 0 };

	render_device.push_target(render_window, false);

	//HorizontalBlurPass hblur(render_device);
	//VerticalBlurPass vblur(render_device);
	//PostProcessEffect blur;
	//blur.passes.push_back(hblur);
	//blur.passes.push_back(vblur);

	// Main loop
	bool quit{ false };
	while (!quit)
	{
		// This part is platform-specific, each OS has its own way to handle windowing ( does not concern the might stegosaurus ) 
		MSG msg;
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{ 
			///////////////////////////////////////////////////////////
			// Rendering with renderers is quite straightforward, you queue 
			// Renderables objects via the render() method 

			// Clears the window to black ( more colors to add ) 
			render_device.clear(render_window, { 0.f, 0.f, 0.f, 0.f }); // THIS METHOD WILL BE MOVED TO THE RENDERER class, not good practice

			// *** RENDER BLOCK ***
			renderer2d.begin(Renderer2D::SortMode::Texture);
			render_device.bind_blender(alpha_blend);

			// Queueing all the sprites we previously created
			renderer2d.render(sprite);

			renderer2d.end();
			// *** RENDER BLOCK ***
			
			// Displays the window
			render_device.finalize(render_window, true);

			///////////////////////////////////////////////////////////
		}

		auto end = std::chrono::high_resolution_clock::now();
		float ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - now).count();

		if (ms >= 1000)
		{
			std::cout << ticks << std::endl;
			ticks = 0;
			now = end;
		}

		++ticks;
	}

	return 0;
}