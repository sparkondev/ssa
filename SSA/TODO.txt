Definire assert che in debug crashano, in release ritornano falso

scrivere anche un abort decente adsa

MODIFY GLM TO SUPPORT INITIALIZER LISTS

Now if you bind a bunch of render targets with p_bind_depth they are ALL bound with depth
We can easily change it ( i think ) 

Convert heades to D3D11.1 Windows 8

Implement blender on MRT

We now use the first depth view of the when binding multiple render targets ( figure out a nice way to work things out)

Make all the call return bool

Rename Texture to Surface

See if it is possible to add const to PostProcessEffect & PostProcessPass vectors

Add checks for surfaces capabilities

Check out how copying / moving renderer works with the locking registration

Move viewport to specific function

Togliere tutti i static dai namespace privati

Add a non_valid resource to reduce overall code size
class Resource
{
public :
	static Resource invalid() { return Resource(bohobh);}
}

///////////////////

// OLD TES
#ifdef SSA_TEST

#include <iostream>

#include "ssa_render_device.hpp"
#include "ssa_entity_framework_api.hpp"
#include "ssa_commander.hpp"
#include "ssa_buffer.hpp"
#include "ssa_shader.hpp"
#include "ssa_system.hpp"
#include "ssa_loaders.hpp"
#include "ssa_sampler.hpp"

#include "2d/ssa_renderer2d.hpp"
#include "2d/ssa_renderable2d.hpp"
#include "2d/ssa_sprite.hpp"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "dxguid.lib")

using namespace SSA;

LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_DESTROY)
		std::exit(0);

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

HWND create_window()
{
	static TCHAR szWindowClass[] = TEXT("win32app");
	static TCHAR szTitle[] = TEXT("SSA");

	WNDCLASS wndClass;
	wndClass.style = CS_DBLCLKS;
	wndClass.lpfnWndProc = StaticWndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = GetModuleHandle(NULL);
	wndClass.hIcon = LoadIcon(nullptr, MAKEINTRESOURCE(IDI_APPLICATION));
	wndClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = nullptr;
	wndClass.lpszClassName = szWindowClass;

	if (!RegisterClass(&wndClass))
	{
		DWORD dwError = GetLastError();
		return nullptr;
	}

	HWND hWnd = CreateWindow(
		szWindowClass,
		szTitle,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		1280, 720,
		NULL,
		NULL,
		GetModuleHandle(NULL),
		NULL
		);

	ShowWindow(hWnd, SW_SHOW);

	if (!hWnd)
		return nullptr;
	return hWnd;
}

struct VertexPosColor
{
	float position[3];
	float color[4];
	float texcoord[2];
};

SSA::Buffer create_triangle_vb(SSA::RenderDevice& p_device)
{
	VertexPosColor vertices[] =
	{
		{ -1.f, -1.f, 0.5f,0.2f, 0.2f, 0.2f, 0.2f,  0.f, 1.f },
		{ -1.f, 1.f, 0.5f, 0.2f, 0.2f, 0.2f, 0.2f, 0.f, 0.f },
		{ 1.f, -1.f, 0.5f, 0.2f, 0.2f, 0.2f, 0.2f, 1.f, 1.f },
		{ 1.f, -1.f, 0.5f, 0.2f, 0.2f, 0.2f, 0.2f, 1.f, 1.f },
		{ -1.f, 1.f, 0.5f, 0.2f, 0.2f, 0.2f, 0.2f, 0.f, 0.f },
		{ 1.f, 1.f, 0.5f, 0.2f, 0.2f, 0.2f, 0.2f, 1.f, 0.f }
	};
	
	SSA::Buffer triangle_vb;
	if (!p_device.create_buffer(SSA::Buffer::Type::Vertex, sizeof(VertexPosColor), 6, false, &vertices[0], "triangle1", triangle_vb))
		std::abort();

	return triangle_vb;
}

static const char* vs_code
{
	"struct VS_INPUT"
	"{"
	"float3 Position : POSITION0;"
	"float4 Color : COLOR0;"
	"float2 TexCoord : TEXCOORD0;"
	"};"

	"struct VS_OUTPUT"
	"{"
	"float4 Position : SV_POSITION0;"
	"float4 Color : COLOR0;"
	"float2 TexCoord : TEXCOORD0;"
	"};"

	"VS_OUTPUT main(VS_INPUT input)"
	"{"
	"VS_OUTPUT output;"
	"output.Position = float4(input.Position, 1.f);"
	"output.TexCoord = input.TexCoord;"
	"output.Color = input.Color;"
	"return output;"
	"}"
};

static const char* ps_code
{
	"cbuffer ColorBuffer"
	"{"
	"float4 color;"
	"};"
	"struct VS_OUTPUT"
	"{"
	"float4 Position : SV_POSITION0;"
	"float4 Color : COLOR0;"
	"float2 TexCoord : TEXCOORD0;"
	"};"
	"Texture2D diffuse_texture : register(t0);"
	"SamplerState diffuse_sampler : register(s0);"

	"float4 main(VS_OUTPUT input) : SV_TARGET"
	"{"
	"return diffuse_texture.Sample(diffuse_sampler, input.TexCoord);"
	"}"
};

#include <random>

int main()
{
	SSA::RenderDevice render_device;
	render_device.init(0);

	HWND hwnd = create_window();

	loaded_data texture_data;
	unsigned long width, height;
	if (!load_png("lol6.png", texture_data, width, height))
		return false;

	loaded_data texture_data2;
	unsigned long width2, height2;
	if (!load_png("lol.png", texture_data2, width2, height2))
		return false;

	SSA::Texture texture;
	if (!render_device.create_texture(width, height, { DXGI_FORMAT_R8G8B8A8_UNORM }, false, &texture_data[0] , "texture_1", texture))
		std::abort();

	SSA::Texture texture2;
	if (!render_device.create_texture(width2, height2, { DXGI_FORMAT_R8G8B8A8_UNORM }, false, &texture_data2[0], "texture_2", texture2))
		std::abort();

	SSA::RenderWindow render_window;
	if (!render_device.create_render_window(hwnd, "render_window_1", render_window))
		std::abort();

	SSA::Shader vertex_shader;
	if (!render_device.create_shader(SSA::Shader::Type::Vertex, vs_code, "main", SSA::Shader::macro_t(), "triangle_vs", vertex_shader))
		std::abort();

	SSA::Shader pixel_shader;
	if (!render_device.create_shader(SSA::Shader::Type::Pixel, ps_code, "main", SSA::Shader::macro_t(), "triangle_ps", pixel_shader))
		std::abort();

	SSA::Sampler sampler;
	if (!render_device.create_sampler("diffuse_sampler", sampler))
		std::abort();

	SSA::Buffer triangle_vb = create_triangle_vb(render_device);
	
	MSG next_message;
	next_message.message = WM_NULL;
	PeekMessage(&next_message, NULL, 0, 0, PM_NOREMOVE);
	TranslateMessage(&next_message);
	DispatchMessage(&next_message);

	bool exit_loop{ false };

	Renderer2D renderer2d(render_device);

	float4 color = { 1.f, 0.3f, 0.5f, 1.f };

	Sprite sprite2;
	sprite2.set_texture(texture2);

	Sprite sprite3;
	sprite3.position = { 100, 100 };

	sprite3.set_texture(texture2);

	Sprite sprite4;
	sprite4.position = { 180, 200 };
	sprite4.set_texture(texture2);

	srand(665);

	std::vector<Sprite> sprites;
	for (unsigned int i{ 0 }; i < 100; ++i)
	{
		Sprite new_sprite;
		new_sprite.set_texture(texture2);
		new_sprite.position.x = rand() % 1280;
		new_sprite.position.y = rand() % 720;
		new_sprite.scale.x = static_cast<float>(rand() % 100) / 100;
		new_sprite.scale.y = static_cast<float>(rand() % 100) / 100;
		new_sprite.rotation = rand() % 360;

		sprites.push_back(new_sprite);
	}

	while (!exit_loop)
	{
		if (PeekMessage(&next_message, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&next_message);
			DispatchMessage(&next_message);
		}
		else
		{
			render_device.clear(render_window);

			render_device.push_target(render_window);

			//render_device.set_value(pixel_shader, "color", &color, sizeof(float)* 4);
			render_device.set_texture(pixel_shader, "diffuse_texture", texture);
			render_device.set_sampler(pixel_shader, "diffuse_sampler", sampler);
			render_device.bind_shader(pixel_shader);

			render_device.bind_buffer(triangle_vb);
			render_device.bind_shader(vertex_shader);
			render_device.bind_shader(pixel_shader);

			//render_device.draw(SSA::PrimitiveTopologyType::TriangleList, 6, 0);

			renderer2d.begin();
		
			for (const auto& sprite : sprites)
				renderer2d.render(sprite);

			renderer2d.end();

			render_device.finalize(render_window);
		}
	}
}

#endif

// TEST 2
///////////////////// Includes
#include <cstdlib>
#include <random>
#include <vector>
#include <ctime>
#include <chrono>
#include <iostream>

#include <Windows.h>

// Don't care about these, will be moved out of here later
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "dxguid.lib")

// Windows's callback
LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_DESTROY)
		std::exit(0);

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

// Creates a window on Windows ( ahaha that joke )
HWND create_window()
{
	static TCHAR szWindowClass[] = TEXT("win32app");
	static TCHAR szTitle[] = TEXT("SSA");

	WNDCLASS wndClass;
	wndClass.style = CS_DBLCLKS;
	wndClass.lpfnWndProc = StaticWndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = GetModuleHandle(NULL);
	wndClass.hIcon = LoadIcon(nullptr, MAKEINTRESOURCE(IDI_APPLICATION));
	wndClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = nullptr;
	wndClass.lpszClassName = szWindowClass;

	if (!RegisterClass(&wndClass))
	{
		DWORD dwError = GetLastError();
		return nullptr;
	}

	HWND hWnd = CreateWindow(
		szWindowClass,
		szTitle,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		1280, 720,
		NULL,
		NULL,
		GetModuleHandle(NULL),
		NULL
		);

	ShowWindow(hWnd, SW_SHOW);

	if (!hWnd)
		return nullptr;
	return hWnd;
}

// Includes will be reduced to a single header per-module
#include "ssa_render_device.hpp"
#include "ssa_loaders.hpp"
#include "ssa_blender.hpp"

#include "2d/ssa_renderer2d.hpp"
#include "2d/ssa_sprite.hpp"

// Application entry point
int main()
{
	// Just for ease of use
	using namespace SSA;

	// We create a window
	HWND window_handle = create_window();

	// The RenderDevice is the main interface for all graphic calls
	RenderDevice render_device;
	if (!render_device.init(0)) // 0 is the index of the graphics card ( usually called adapter ) [ other adapters are not supported yet, parameter is just ignored for now ]
		return false;

	// Let's create a texture
	// We are using a module ( contained in ssa_loaders.hpp ) that supports only png for now
	// We first load it
	loaded_data texture_data;
	unsigned long width, height;
	if (!load_png("sample_texture.png", texture_data, width, height))
		return false;

	loaded_data texture_data2;
	unsigned long width2, height2;
	if (!load_png("to_bloom.png", texture_data2, width2, height2))
		return false;

	// Then use the data obtained ( raw pixels ) to create it
	Texture texture; // This is just the handle you will use ( also contains informations about the texture ) 
	// If you wanna know more about this function, well just don't care for now, there is no CrossPlatform Format system yet ( sorry )
	if (!render_device.create_texture(width, height, { DXGI_FORMAT_R8G8B8A8_UNORM }, false, &texture_data[0], "texture_data", texture))
		return false;

	Texture texture2;
	if (!render_device.create_texture(width2, height2, { DXGI_FORMAT_R8G8B8A8_UNORM }, false, &texture_data2[0], "texture_data2", texture2))
		return false;

	// We have created a window, but as with the texture, the window itself means nothing to the might Stegosaurus
	// That's why you have to convert it to a Stegosaurus object
	RenderWindow render_window; // A RenderWindow is a window where you render onto ( uahuuu ) 
	if (!render_device.create_render_window(window_handle, "my_super_special_window", render_window))
		return false;

	// We now create a Renderer2D that has the duty to render things on screen
	// We will later have multiple renderers for different purposes ( deferred, prepass, 2d, 2.5d & more ) 
	Renderer2D renderer2d(render_device);

	Blender alpha_blend;
	if (!render_device.create_blender("blender", alpha_blend))
		return false;

	// We now create a bunch of sprites ( 2d textured-quads )
	std::vector<Sprite> sprites;
	std::vector<float2> velocities;
	for (unsigned int i{ 0 }; i < 1000; ++i)
	{
		Sprite new_sprite;

		// Setting some sprite infos ( why texture has a set_* method and other components don't ? good question! ) 
		if (i % 2 == 0)
			new_sprite.set_texture(texture);
		else
			new_sprite.set_texture(texture2);

		new_sprite.position.x = rand() % 1280 ;
		new_sprite.position.y = rand() % 720 ;
		//new_sprite.scale.x = static_cast<float>(rand() % 100) / 100;
		//new_sprite.scale.y = static_cast<float>(rand() % 100) / 100;
		new_sprite.scale.x = new_sprite.scale.y = 0.3f;
		// new_sprite.rotation = rand() % 360;
		new_sprite.set_color(float4{ static_cast<float>(rand() % 100) / 100,
			static_cast<float>(rand() % 100) / 100,
			static_cast<float>(rand() % 100) / 100, 1.f });

		sprites.push_back(new_sprite);

		velocities.push_back(float2{ static_cast<float>(rand() % 100 - 50) / 100, static_cast<float>(rand() % 100 - 50) / 100 });
	}

	// This is quite hard to explain how the internal render target stack works, just assume that
	// this call puts the render target on top of a rendering stack
	render_device.push_target(render_window);

	static VertexPCT default_vertices[] =
	{
		{ float3{ 0.f, 720.f, 0.5f }, float4{ 0.3f, 1.f, 1.f, 1.f }, 0.f, 0.f },
		{ float3{ 200.f, 360.f, 0.5f }, float4{ 1.f, 0.f, 0.4f, 1.f }, 1.f, 0.f },
		{ float3{ 1280.f / 2.f - 30, 50.f, 0.5f }, float4{ 0.5f, 1.f, 0.f, 1.f }, 1.f, 1.f },
		{ float3{ 1280.f / 2.f, 0.f, 0.5f }, float4{ 1.f, 1.f, 1.f, 1.f }, 1.f, 1.f },
		{ float3{ 1280.f / 2.f + 30, 50.f, 0.5f }, float4{ 1.f, 0.f, 1.f, 1.f }, 1.f, 1.f },
		{ float3{ 1280.f - 200, 360.f, 0.5f }, float4{ 0.f, 0.3f, 0.2f, 1.f }, 0.f, 1.f },
		{ float3{ 1280.f, 720.f, 0.5f }, float4{ 1.f, 0.2f, 0.7f, 1.f }, 0.f, 1.f }
	};

	Renderable2D renderable;
	renderable.vertices.push_back(default_vertices[0]);
	renderable.vertices.push_back(default_vertices[1]);
	renderable.vertices.push_back(default_vertices[2]);
	renderable.vertices.push_back(default_vertices[3]);
	renderable.vertices.push_back(default_vertices[4]);
	renderable.vertices.push_back(default_vertices[5]);
	renderable.vertices.push_back(default_vertices[6]);
	renderable.triangulate();

	auto now = std::chrono::high_resolution_clock::now();
	unsigned int ticks{ 0 };

	// Main loop
	bool quit{ false };
	while (!quit)
	{
		// This part is platform-specific, each OS has its own way to handle windowing ( does not concern the might stegosaurus ) 
		MSG msg;
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{ 
			///////////////////////////////////////////////////////////
			// Rendering with renderers is quite straightforward, you queue 
			// Renderables objects via the render() method 

			// Clears the window to black ( more colors to add ) 
			render_device.clear(render_window); // THIS METHOD WILL BE MOVED TO THE RENDERER class, not good practice

			// Let's move the sprites around a bit
			//srand(time(NULL));
			for (unsigned int i{ 0 }; i < velocities.size(); ++i)
			{
				sprites[i].position.x += velocities[i].x * 0.16f * 50;
				sprites[i].position.y += velocities[i].y * 0.16f * 50;

				// Invert direction when they hit walls
				if (sprites[i].position.x >= 1280 || sprites[i].position.x <= 0)
					velocities[i].x = -velocities[i].x;
				if (sprites[i].position.y >= 720 || sprites[i].position.y <= 0)
					velocities[i].y = -velocities[i].y;
			}

			// *** RENDER BLOCK ***
			renderer2d.begin(Renderer2D::SortMode::Texture);
			render_device.bind_blender(alpha_blend);

			// Queueing all the sprites we previously created
			for (const auto& sprite : sprites)
				renderer2d.render(sprite);
			renderer2d.render(renderable);

			renderer2d.end();
			// *** RENDER BLOCK ***
			
			// Displays the window
			render_device.finalize(render_window);

			///////////////////////////////////////////////////////////
		}

		auto end = std::chrono::high_resolution_clock::now();
		float ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - now).count();

		if (ms >= 1000)
		{
			std::cout << ticks << std::endl;
			ticks = 0;
			now = end;
		}

		++ticks;
	}
}