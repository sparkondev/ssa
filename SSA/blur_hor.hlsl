Texture2D render_target : register(r0)
Sampler render_target_sampler : register(s0)

struct VS_OUTPUT
{
	float4 Position : SV_POSITION0;
	float2 TexCoord : TEXCOORD0;
}

cbuffer BlurData
{
	float blur_size;
}

float4 main(VS_OUTPUT input) : SV_TARGET
{
	float4 final_color(0.f, 0.f, 0.f, 0.f);

	final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x - 4.0 * blur_size, input.TexCoord.y));

	final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x - 3.0 * blur_size, input.TexCoord.y));

	final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x - 2.0 * blur_size, input.TexCoord.y));

	final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x - blur_size, input.TexCoord.y));

	final_color += render_target.Sample(render_target_sampler, input.TexCoord);
	
	final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x + blur_size, input.TexCoord.y));

	final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x + 2.0 * blur_size, input.TexCoord.y));

	final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x + 3.0 * blur_size, input.TexCoord.y));

	final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x + 4.0 * blur_size, input.TexCoord.y));

	return final_color / 9;
}