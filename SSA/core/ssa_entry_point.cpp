/*
The MIT License(MIT)

Copyright(c) 2014 Edoardo 'sparkon' Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Header
#include "ssa_entry_point.hpp"

#if defined(ssa_os_windows)

	int main(int argc, char* argv[])
	{
		cmd_args_t cmd_args;

		// Format arguments
		for (int i{ 0 }; i < argc; ++i)
			cmd_args.push_back(argv[i]);

		return entry_point(cmd_args, SSA::Platform::Win32);
	}

#include <shellapi.h>

	int WINAPI WinMain(HINSTANCE p_instance, HINSTANCE p_prev_instance, LPSTR p_cmd_line, int p_cmd_show)
	{
		cmd_args_t cmd_args;

		std::string cmd_line{ p_cmd_line };
		std::wstring wcmd_line;
		wcmd_line.assign(cmd_line.begin(), cmd_line.end());

		int num_args{ 0 };
		auto args = CommandLineToArgvW(wcmd_line.c_str(), &num_args);

		// Format arguments
		for (int i{ 0 }; i < num_args; ++i)
		{
			std::wstring warg{ args[0] };
			std::string arg;
			arg.assign(warg.begin(), warg.end());
		}

		auto result = entry_point(cmd_args, SSA::Platform::Win32);
		LocalFree(args);

		return result;
	};

#elif defined(ssa_os_windows_phone)

#elif defined(ssa_os_linux)

#elif defined(ssa_os_macos)

#endif