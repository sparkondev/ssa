/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// C++ STD
#include <cstdlib>
#include <cstdint>
#include <list>
#include <cassert>

namespace SSA
{

	class Bag
	{
	public:
		typedef std::uint64_t index_t;

	public:
		//! Constructs a new instance of Bag and allocates the initial buffer
		//! \param p_element_size
		//! \param p_initial_buffer_size
		Bag(std::size_t p_element_size, std::size_t p_initial_buffer_size);

		//! Deallocates all internal memory
		~Bag();

		template <typename object_t, typename ...ctor_args>
		index_t add_object(ctor_args ...p_args);

		template <typename object_t>
		object_t& get_object(index_t p_index);

		void recycle(index_t p_index);

		uint8_t* get_data_ptr()					{ return m_data; }
		std::size_t get_element_size()const		{ return m_element_size; }
		index_t get_last_element_pos()const 
		{ 
			return m_current_size; 
		}

	private:
		void _safe_release();

		// Resizes array if necessary
		index_t _get_next_spot();

	private:
		// List of free spots in the buffer
		std::list<index_t>	m_free_list;

		// Actual buffer
		uint8_t*				m_data;

		// Size of the single item in the buffer
		std::size_t			m_element_size;
		index_t				m_current_size;
	};

	// === TEMPLATE ===
	template <typename object_t, typename ...ctor_args>
	Bag::index_t Bag::add_object(ctor_args ...p_args)
	{
		assert(m_data != nullptr);

		// Taking the first free spot
		index_t next_spot = _get_next_spot();

		// Constructing object & copying it
		object_t object{p_args...};
		std::memcpy(m_data + next_spot * m_element_size,
			reinterpret_cast<void*>(&object),
			sizeof(object));

		return next_spot;
	}

	template <typename object_t>
	object_t& Bag::get_object(Bag::index_t p_index)
	{
		assert(m_data != nullptr);

		return *reinterpret_cast<object_t*>(m_data + p_index * m_element_size);
	}
}