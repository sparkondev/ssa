﻿/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// Stegosaurus version
#define ssa_version_major 0
#define ssa_version_minor 1

// Compiler macos
#if defined(_MSC_VER)
#define ssa_compiler_msvc
#elif defined(__GNUC__)
#define ssa_compiler_gcc
#elif defined(__clang__)
#define ssa_compiler_clang
#else
#error The compiler you are currently building with is not supported, see requirements.txt for more informations
#endif

#if !defined(__cplusplus)
#error C++11 compiler needed, see requirements.txt for more informations
#endif 

// Platform macros
// Reserved for Windows 8 + // @TODO : 
#include <winapifamily.h>

#if WINAPI_FAMILY == WINAPI_FAMILY_DESKTOP_APP
#include <windows.h>
#define ssa_os_windows
#elif WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP
#define ssa_os_windows_phone
#elif defined(__linux__)
#define ssa_os_linux
#error All linux-based operating systems are not supported yet
#elif defined(macintosh) || defined(Macintosh) || (defined(__APPLE__) && defined(__MACH__))
#define ssa_os_macos
#error All MacOses are not supported yet
#else
#error Operating system not recognized and probably not supported, see requirements.txt for more informations
#endif

// Architecture ( more to add ), we don't care  about the compiler here
#if defined(ssa_os_windows) && defined(_WIN64)
#define ssa_arch_64
#elif defined(__amd64__) || defined(_M_X64)
#define ssa_arch_64
#else 
#define SSA_ARCH_32
#endif

// Language specific 
#if defined(ssa_compiler_msvc)
#define ssa_force_inline __forceinline
#elif defined(ssa_compiler_gcc)
#define ssa_force_inline __attribute__((always_inline))
#elif defined(ssa_compiler_clang)
#define ssa_force_inline
#else
#define ssa_force_inline
#endif

namespace SSA
{
	enum class Platform
	{
		Win32, // Desktop
		WinRT  // Mobile
	};
}

#define ssa_constexpr 