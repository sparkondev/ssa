/*
The MIT License(MIT)

Copyright(c) 2014 Edoardo 'sparkon' Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// SSA
#include "ssa_render_device.hpp"
#include "ssa_post_process_effect.hpp"

// C++ STD
#include <vector>
#include <utility>

namespace SSA
{
	//! \brief Base class for every renderer, it simplifies syncronization 
	//!		and helps not overlapping state setting and not screwing up rendering
	//!
	//! > What is the idea of a renderer ?
	//! 
	//! > How does locking works ? 
	class Renderer
	{
	public:
		//! \brief Constructs a new Renderer instance
		//! 
		//! \param [in] p_render_device RenderDevice that will be used by the renderer for rendering, this *HAS* to be initialized
		//!		via init() before passing it, it might cause crashes otherwise
		//! \param [in] p_self This one is odd, this should be *this a reference to itself, this is needed
		//!		for registration / locking purposes
		Renderer(RenderDevice& p_render_device, const Renderer& p_self);

		//! \brief Default destructor, does not destroy associated resources
		~Renderer();

		//! \brief Returns the RenderDevice used to initialize the Renderer
		//! \return Reference to the RenderDevice used to initialize this Renderer
		RenderDevice& get_render_device()const { return m_render_device; }

		///////////////////////////////////////////////////////////////////////
		/// LOCKING
		///////////////////////////////////////////////////////////////////////

		//! \brief Checks if the renderer can begin a rendering session and not allow other
		//!		Renderers to render. It also locks the session itself
		//! \return True if session can begin and it locks it, false otherwise
		bool can_begin();

		//! \brief Checks if session has been locked by himself
		//! \return True if session belongs to him, false otherwise
		bool can_render();

		//! \brief Checks if session has been locked by himself, and if so, it releases it
		//!		leaving it open for other Rendereres to start rendering
		//! \return True if session has been locked by him and unlocks it, false otherwise
		bool can_end();

		///////////////////////////////////////////////////////////////////////
		/// POST PROCESSING
		///////////////////////////////////////////////////////////////////////
		void push_effect(PostProcessEffect& p_effect);

		void pop_effect();

	protected :
		RenderDevice& m_render_device;
	
		// Do postprocessing
		void _post_process(Texture& p_render_target);

	private :
		static bool				s_locked;
		static Renderer const*	s_locker;

		Renderer const* m_self;

		// Postprocessing stuff
		std::vector<std::reference_wrapper<PostProcessEffect>> m_pp_effects;
	};
}