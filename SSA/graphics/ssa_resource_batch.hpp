/*
The MIT License(MIT)

Copyright(c) 2014 Edoardo 'sparkon' Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// C++ STD
#include <algorithm>
#include <vector>
#include <list>

namespace SSA
{
	template <typename resource_t>
	class ResourceBatch
	{
	public:
		typedef std::size_t index_t;
	public:
		ResourceBatch(std::size_t p_initial_size);
		~ResourceBatch();

		index_t create_resource();

		resource_t& get(index_t p_index);

		void recycle(index_t p_index);

	private:
		std::vector<resource_t>						m_resources;
		std::list<index_t>							m_free_list;
	};

	template <typename resource_t>
	ResourceBatch<resource_t>::ResourceBatch(std::size_t p_initial_size)
	{
		m_resources.resize(p_initial_size);

		m_free_list.resize(m_resources.size());
		unsigned int counter{ 0 };
		std::generate(m_free_list.begin(), m_free_list.end(), [&] { return counter++; });
	}

	template <typename resource_t>
	ResourceBatch<resource_t>::~ResourceBatch()
	{
		// @TODO : implement deallocation callback
	}

	template <typename resource_t>
	typename ResourceBatch<resource_t>::index_t ResourceBatch<resource_t>::create_resource()
	{
		index_t next_free = m_free_list.front();
		m_free_list.pop_front();

		return next_free;
	}

	template <typename resource_t>
	resource_t& ResourceBatch<resource_t>::get(typename ResourceBatch<resource_t>::index_t p_index)
	{
		return m_resources[p_index];
	}

	template <typename resource_t>
	void ResourceBatch<resource_t>::recycle(typename ResourceBatch<resource_t>::index_t p_index)
	{
		// @TODO : implement deallocation callback
		m_free_list.push_front(p_index);
	}
}