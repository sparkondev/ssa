/*
The MIT License(MIT)

Copyright(c) 2014 Edoardo 'sparkon' Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// SSA 
#include "ssa_resource.hpp"
#include "ssa_commander.hpp"

namespace SSA
{
	class Buffer : public Resource
	{
		friend class RenderDevice;
	
	// Just renaming some enumerators
	public:
		typedef BufferType Type;

	public:
		Buffer(RenderDevice& p_render_device) :
			Resource{ m_render_device } { } 
		virtual ~Buffer() = default;

		//! \brief Returns the type of the buffer
		//! \return Type of the buffer
		Type get_type()const { return m_type; }

		//! \brief Returns the size of a single element in the buffer
		//! \return Size of a single element in the buffer ( usually called stride )
		std::size_t get_stride()const { return m_stride; }

	protected :
		Type		m_type;
		std::size_t m_stride;
	};
}