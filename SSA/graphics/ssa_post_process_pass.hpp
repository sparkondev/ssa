/*
The MIT License(MIT)

Copyright(c) 2014 Edoardo 'sparkon' Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// Includes
#include "ssa_shader.hpp"

namespace SSA
{
	// Forward declarations
	class RenderDevice;

	//! \brief Class that represents a single post-process pass ( full-screen render + pixel shader ) 
	class PostProcessPass
	{
	public :
		PostProcessPass(RenderDevice& p_render_device) :
			m_render_device{ p_render_device },
			m_shader{ p_render_device } { } 

		const Shader& get_shader()const { return m_shader; }

	protected :
		RenderDevice&	m_render_device;

		Shader			m_shader;
	};
}