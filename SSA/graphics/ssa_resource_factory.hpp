/*
The MIT License(MIT)

Copyright(c) 2014 Edoardo 'sparkon' Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// SSA
#include "ssa_resource_batch.hpp"
#include "ssa_commander.hpp"

namespace SSA
{
	class ResourceFactory
	{
	public:
		ResourceFactory(Commander& p_commander);
		~ResourceFactory();

		ResourceBatch<TextureInternal>::index_t create_texture();
		TextureInternal& get_texture(ResourceBatch<TextureInternal>::index_t p_id);
		void destroy_texture(ResourceBatch<TextureInternal>::index_t p_id);

		ResourceBatch<BufferInternal>::index_t create_buffer();
		BufferInternal& get_buffer(ResourceBatch<BufferInternal>::index_t p_id);
		void destroy_buffer(ResourceBatch<BufferInternal>::index_t p_id);

		ResourceBatch<ShaderInternal>::index_t create_shader();
		ShaderInternal& get_shader(ResourceBatch<ShaderInternal>::index_t p_id);
		void destroy_shader(ResourceBatch<ShaderInternal>::index_t p_id);

		ResourceBatch<SamplerInternal>::index_t create_sampler();
		SamplerInternal& get_sampler(ResourceBatch<SamplerInternal>::index_t p_id);
		void destroy_sampler(ResourceBatch<SamplerInternal>::index_t p_id);

		ResourceBatch<BlenderInternal>::index_t create_blender();
		BlenderInternal& get_blender(ResourceBatch<BlenderInternal>::index_t p_id);
		void destroy_blender(ResourceBatch<BlenderInternal>::index_t p_id);

	private:
		Commander& m_commander;

		ResourceBatch<TextureInternal>			m_textures;
		ResourceBatch<BufferInternal>			m_buffers;
		ResourceBatch<ShaderInternal>			m_shaders;
		ResourceBatch<SamplerInternal>			m_samplers;
		ResourceBatch<BlenderInternal>			m_blenders;
	};
}