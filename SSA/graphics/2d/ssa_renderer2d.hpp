/*
The MIT License(MIT)

Copyright(c) 2014 Edoardo 'sparkon' Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// SSA
#include "../ssa_renderer.hpp"
#include "../ssa_buffer.hpp"
#include "../ssa_common_vertex_formats.hpp"
#include "../ssa_shader.hpp"
#include "../ssa_sampler.hpp"

namespace SSA
{
	class RenderDevice;
	class Renderable2D;

	class Renderer2D : public Renderer
	{
		static const std::size_t default_buffer_size{ 1296 };

		// @TODO : implement multiple texture binding
		// static const std::size_t max_bound_textures{ 16 };
	
	public :
		enum class SortMode
		{
			None,
			Texture,
			FrontToBack,
			BackToFront,
		};

	public :
		Renderer2D(RenderDevice& p_render_device, std::size_t p_initial_buffer_size = default_buffer_size);
		~Renderer2D();

		void begin(SortMode p_sort_mode);

		// Not const cause it might trigger renderable triangulation
		void render(Renderable2D& p_renderable);
		void end();

	protected :
		void _flush();
		void _set_states();

	private :
		SortMode	m_sort_mode;

		Buffer		m_graphics_buffer;
		VertexPCT*	m_raw_buffer;
		std::vector<std::reference_wrapper<Renderable2D>> m_renderables;

		std::size_t m_buffer_size;
		std::size_t m_last_element;

		Shader		m_vertex_shader;
		Shader		m_pixel_shader;
		Sampler		m_texture_sampler;
	};
}