/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Header
#include "ssa_sprite.hpp"

// SSA
#include "../ssa_texture.hpp"

namespace SSA
{
	namespace
	{
		const VertexPCT default_vertices[] =
		{
			{ float3{ 0.f, 0.f, 0.5f }, float4{ 1.f, 1.f, 1.f, 1.f }, 0.f, 0.f },
			{ float3{ 0.f, 0.f, 0.5f }, float4{ 1.f, 1.f, 1.f, 1.f }, 1.f, 0.f },
			{ float3{ 0.f, 0.f, 0.5f }, float4{ 1.f, 1.f, 1.f, 1.f }, 1.f, 1.f },
			{ float3{ 0.f, 0.f, 0.5f }, float4{ 1.f, 1.f, 1.f, 1.f }, 0.f, 1.f }
		};
	}

	Sprite::Sprite()
	{
		set_vertices(&default_vertices[0], 4);
	}

	void Sprite::set_texture(const Texture& p_texture)
	{
		Renderable2D::set_texture(p_texture);

		// Resizing vertices, manually operating on them
		// Upper-Right corner
		m_vertices[1].position[0] = static_cast<float>(m_texture->get_data().width);

		// Lower-Right corner
		m_vertices[2].position[0] = static_cast<float>(m_texture->get_data().width);
		m_vertices[2].position[1] = static_cast<float>(m_texture->get_data().height);

		// Lower-Left corner
		m_vertices[3].position[1] = static_cast<float>(m_texture->get_data().height);

		// Manually calling triangulate
		Renderable2D::_triangulate();
	}
}