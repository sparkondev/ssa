/*
The MIT License(MIT)

Copyright(c) 2014 Edoardo 'sparkon' Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// SSA
#include "../../core/ssa_math.hpp"

namespace SSA
{
	//! \brief Class that wraps all supported transformations in 2D space ( more to add ) 
	struct Transformable2D
	{
		Transformable2D() :
		position{ 0 },
		scale{ 1 },
		origin{ 0 },
		rotation{ 0 } { } 

		//! \brief Position defined in pixel coordinates, X-axis goes from left to right ( positive ) and from top to bottom ( positive ) 
		//!		origin is situated in the top-left corner of the window
		float2 position;

		//! \brief Scale of the object along the X and Y axis, defaults to 1.f for no scaling
		float2 scale;

		//! \brief Origin for transformation, before object is transformed it displaced by its origin, transformed and then moved back
		//!		particularly useful for rotations. It's defined in coordinates relative from the position of the object
		float2 origin;

		//! \brief Rotation along the Z-axis
		float  rotation;
	};
}