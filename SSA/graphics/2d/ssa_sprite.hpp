/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// SSA
#include "ssa_renderable2d.hpp"

namespace SSA
{
	//! \brief Also known as PointSprite it's a simple textured-quad and since it's the
	//!		most common shape indeed, is a semplification of a Renderable2D
	class Sprite : public Renderable2D
	{
	public :
		//! \brief Creates a new sprite of size (1, 1)
		Sprite();

		//! \brief Sets a new texture and new dimensions for the sprite
		//! 
		//! This call does an additional operation by resizing the sprite to the texture's dimensions
		//! \param [in] p_texture New texture
		void set_texture(const Texture& p_texture);
	};
}