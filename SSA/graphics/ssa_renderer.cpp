/*
The MIT License(MIT)

Copyright(c) 2014 Edoardo 'sparkon' Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Header
#include "ssa_renderer.hpp" 

// SSA
#include "ssa_texture.hpp"

namespace SSA
{
	bool			Renderer::s_locked{ false };
	Renderer const* Renderer::s_locker{ nullptr };

	Renderer::Renderer(RenderDevice& p_render_device, const Renderer& p_self) :
		m_render_device{ p_render_device },
		m_self{ &p_self }
	{

	}

	Renderer::~Renderer()
	{

	}

	///////////////////////////////////////////////////////////////////////
	/// LOCKING
	///////////////////////////////////////////////////////////////////////

	bool Renderer::can_begin()
	{
		if (s_locked && (s_locker != m_self))
			return false;

		s_locked = true;
		s_locker = m_self;
		return true;
	}

	bool Renderer::can_render()
	{
		if (!s_locked || (s_locker != m_self))
			return false;

		return true;
	}

	bool Renderer::can_end()
	{
		if (!s_locked ||
			s_locker != m_self)
			return false;

		s_locked = false;
		s_locker = nullptr;

		return true;
	}

	///////////////////////////////////////////////////////////////////////
	/// POST PROCESSING
	///////////////////////////////////////////////////////////////////////
	void Renderer::push_effect(PostProcessEffect& p_effect)
	{
		m_pp_effects.push_back(p_effect);
	}

	void Renderer::pop_effect()
	{
		//@ TODO: Fix all this assert() thing
		assert(!m_pp_effects.empty());

		if (!m_pp_effects.empty())
			m_pp_effects.pop_back();
	}

	void Renderer::_post_process(Texture& p_render_target)
	{
		if (m_pp_effects.empty())
			return;

		// Now we are executing the postprocessing effects
		for (auto& effect : m_pp_effects)
		{
			// p_render_target should not be bound to the pipeline, it should be the result of previous rendering operation
			
		}
	}
}