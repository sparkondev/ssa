/*
The MIT License(MIT)

Copyright(c) 2014 Edoardo 'sparkon' Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Header
#include "ssa_pp_horizontal_blur_pass.hpp"

// SSA
#include "../ssa_render_device.hpp"

namespace SSA
{
	namespace
	{
		const std::string horizontal_blur_ps_code
		{
		"Texture2D render_target : register(r0)"
		"Sampler render_target_sampler : register(s0)"
		"struct VS_OUTPUT"
		"	{"
		"float4 Position : SV_POSITION0;"
		"float2 TexCoord : TEXCOORD0;"
		"};"

		"cbuffer BlurData"
		"{"
		"float blur_size;"
		"};"
		"float4 main(VS_OUTPUT input) : SV_TARGET"
		"{"
		"float4 final_color(0.f, 0.f, 0.f, 0.f);"

		"final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x - 4.0 * blur_size, input.TexCoord.y));"

		"final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x - 3.0 * blur_size, input.TexCoord.y));"

		"final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x - 2.0 * blur_size, input.TexCoord.y));"

		"final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x - blur_size, input.TexCoord.y));"

		"final_color += render_target.Sample(render_target_sampler, input.TexCoord);"

		"final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x + blur_size, input.TexCoord.y));"

		"final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x + 2.0 * blur_size, input.TexCoord.y));"

		"final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x + 3.0 * blur_size, input.TexCoord.y));"

		"final_color += render_target.Sample(render_target_sampler, float2(input.TexCoord.x + 4.0 * blur_size, input.TexCoord.y));"

		"return final_color / 9;"
		"}"
		};
	}

	HorizontalBlurPass::HorizontalBlurPass(RenderDevice& p_render_device) :
		PostProcessPass{ p_render_device }
	{
		if (!p_render_device.create_shader(Shader::Type::Pixel, horizontal_blur_ps_code, "main", Shader::macro_t(), "Horizontal Blur Default Shader", m_shader))
			std::abort();
	}
}