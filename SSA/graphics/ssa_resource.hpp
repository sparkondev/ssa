/*
The MIT License(MIT)

Copyright(c) 2014 Edoardo 'sparkon' Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// C++ STD
#include <string>

namespace SSA
{
	//! \brief Base class for every resource handlers, it is simply a container
	//!		for an id or a name
	class Resource
	{
		friend class RenderDevice;
	public:
		Resource(RenderDevice& p_render_device) :
			m_render_device{ p_render_device } { } 
		virtual ~Resource() = default;


		//! \brief Returns the string that identifies the resource 
		//! \return Name identifier of the resource
		const std::string& get_name()const { return m_name; }

		//! \brief Returns the id that identifies the resource
		//! \return ID of the resource
		std::size_t get_id()const { return m_id; }

	protected:
		RenderDevice& m_render_device;
		std::size_t m_id;
		std::string m_name;
	};
}