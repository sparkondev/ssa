/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Header
#include "ssa_entity_framework_api.hpp"

namespace SSA
{
	EntityFrameworkAPI::EntityFrameworkAPI() : 
		m_entity_factory{},
		m_component_factory{},
		m_system_looper{ m_entity_factory, m_component_factory }
	{

	}

	EntityFrameworkAPI::~EntityFrameworkAPI()
	{

	}

	// ===== ENTITY-RELATED METHODS =====
	EntityHandle EntityFrameworkAPI::create_entity()
	{
		return EntityHandle(m_entity_factory.create_entity(), m_component_factory);
	}

	EntityHandle EntityFrameworkAPI::get_entity(Entity::id_t p_id)
	{
		return EntityHandle(m_entity_factory.get_entity(p_id), m_component_factory);
	}

	void EntityFrameworkAPI::process()
	{
		m_system_looper.process();
	}
}