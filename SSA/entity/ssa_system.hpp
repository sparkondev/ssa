﻿/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// C++ STD
#include <bitset>

// C++ STD
#include "ssa_component.hpp"

namespace SSA
{
	// Forward declarations
	class EntityHandle;
	class ComponentFactory;

	class System
	{
		friend class SystemLooper;
	public:
		System() : m_enabled{ false } { } 
		~System() = default;

		template <typename component_t>
		void register_component();

		template <typename component_t>
		void unregister_component();

		template <typename component_t>
		bool is_component_registered();

		bool is_component_registered(Component::id_t p_id)const { return m_registered_components[static_cast<std::size_t>(p_id)]; }

		const std::bitset<Component::max_component_number>& get_registered_all()const { return m_registered_components; }

		virtual void preprocess() = 0;
		virtual void process(EntityHandle& p_next_entity) = 0;
		virtual void finalize() = 0;

		void enable() { m_enabled = true; }
		void disable() { m_enabled = false; }
		bool is_enabled()const { return m_enabled; }

	protected:
		bool			m_enabled;
		std::bitset<Component::max_component_number> m_registered_components;
	
	private :
		ComponentFactory* m_component_factory;
	};

	template <typename component_t>
	void System::register_component()
	{
		m_registered_components.set(static_cast<std::size_t>(m_component_factory->get_type_from_component<component_t>()), true);
	}

	template <typename component_t>
	void System::unregister_component()
	{
		m_registered_components.set(m_component_factory->get_type_from_component<component_t>(), false);
	}

	template <typename component_t>
	bool System::is_component_registered()
	{
		return m_registered_components[m_component_factory->get_type_from_component<component_t>()];
	}
}