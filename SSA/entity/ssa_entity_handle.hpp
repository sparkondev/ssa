/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#pragma once

// SSA
#include "ssa_component.hpp"
#include "ssa_entity.hpp"

namespace SSA
{
	// Forward declaration
	class ComponentFactory;

	class EntityHandle
	{
	public:
		EntityHandle(Entity& e, ComponentFactory& p_component_factory);
		EntityHandle(const EntityHandle& p_other);
		EntityHandle& operator=(const EntityHandle& p_other);
		~EntityHandle();

		Entity& get() { return *m_entity; }

		Entity::id_t get_id()const { return m_entity->id; }

		template <typename component_t>
		component_t& get_component();

		template <typename component_t, typename ...ctor_args_t>
		void attach_component(ctor_args_t ...p_ctor_args);

	private:
		ComponentFactory*	m_component_factory;
		Entity*				m_entity;
	};

	template <typename component_t>
	component_t& EntityHandle::get_component()
	{
		return m_entity->get_component<component_t>(m_component_factory->get_type_from_component<component_t>());
	}

	template <typename component_t, typename ...ctor_args_t>
	void EntityHandle::attach_component(ctor_args_t ...p_ctor_args)
	{
		Component::id_t new_component_id = m_component_factory->attach_component<component_t>(*this, p_ctor_args...);
		m_entity->add_component(&m_component_factory->get_component<component_t>(new_component_id), m_component_factory->get_type_from_component<component_t>());
	}
}