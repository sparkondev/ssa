/*
The MIT License(MIT)

Copyright(c) 2014 Angelini, Bracci, Dominici

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files(the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Header
#include "ssa_entity_factory.hpp"

namespace SSA
{
	EntityFactory::EntityFactory() :
		m_entities(sizeof(Entity), 50)
	{
	}

	Entity& EntityFactory::create_entity()
	{
		Entity::id_t id = m_entities.add_object<Entity>();
		Entity& new_entity = m_entities.get_object<Entity>(id);
		new_entity.id = id;
		return new_entity;
	}

	Entity& EntityFactory::get_entity(Entity::id_t p_id)
	{
		return m_entities.get_object<Entity>(p_id);
	}

	void EntityFactory::remove_entity(Entity& p_entity)
	{
		p_entity.ref_count--;
		if (p_entity.ref_count <= 0)
			m_entities.recycle(p_entity.id);
	}
}